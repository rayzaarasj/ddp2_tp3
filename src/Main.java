import java.util.Scanner;

/**
 * Created by rayzaarasj on 25/04/2017.
 */
public class Main {
	public static void main(String[] args) {
        Owjek regular = new Regular();
        Owjek sporty = new Sporty();
        Owjek exclusive = new Exclusive();

        Map map;

        Scanner s = new Scanner(System.in);

        String input = "";
        String[] inputArray;

		System.out.println("\n" +
				"  /$$$$$$                                       /$$$$$           /$$              /$$$$$$                                 /$$                    \n" +
				" /$$__  $$                                     |__  $$          | $$             /$$__  $$                               |__/                    \n" +
				"| $$  \\ $$ /$$  /$$  /$$                          | $$  /$$$$$$ | $$   /$$      | $$  \\__/  /$$$$$$   /$$$$$$  /$$    /$$ /$$  /$$$$$$$  /$$$$$$ \n" +
				"| $$  | $$| $$ | $$ | $$       /$$$$$$            | $$ /$$__  $$| $$  /$$/      |  $$$$$$  /$$__  $$ /$$__  $$|  $$  /$$/| $$ /$$_____/ /$$__  $$\n" +
				"| $$  | $$| $$ | $$ | $$      |______/       /$$  | $$| $$$$$$$$| $$$$$$/        \\____  $$| $$$$$$$$| $$  \\__/ \\  $$/$$/ | $$| $$      | $$$$$$$$\n" +
				"| $$  | $$| $$ | $$ | $$                    | $$  | $$| $$_____/| $$_  $$        /$$  \\ $$| $$_____/| $$        \\  $$$/  | $$| $$      | $$_____/\n" +
				"|  $$$$$$/|  $$$$$/$$$$/                    |  $$$$$$/|  $$$$$$$| $$ \\  $$      |  $$$$$$/|  $$$$$$$| $$         \\  $/   | $$|  $$$$$$$|  $$$$$$$\n" +
				" \\______/  \\_____/\\___/                      \\______/  \\_______/|__/  \\__/       \\______/  \\_______/|__/          \\_/    |__/ \\_______/ \\_______/\n" +
				"                                                                                                                                                 \n");
		System.out.println("Welcome to Ow-Jek Service, where your trip is our priority!");
		System.out.println("Type -h to see all the commands available");
		System.out.println("\nHappy Ow-Jek-ing!");

        while (!input.equals("EXIT")) {
			System.out.print(">> ");
			input = s.nextLine();
        	inputArray = input.split(" ");

        	switch (inputArray[0].toUpperCase()) {
				case "SHOW" :
					if (inputArray.length >= 2 && inputArray[1].toUpperCase().equals("MAP")) {
						map = new Map();
						map.print();
					} else {
						System.out.println("Invalid Input : show command not found");
					}
					break;

				case "GO" :
					if (inputArray.length == 8) {
						map = new Map();
						if (checkMap(inputArray[2], inputArray[4], map)) {
							if (!inputArray[2].equals(inputArray[4])) {
								String from = inputArray[2];
								String to = inputArray[4];
								switch (inputArray[7].toUpperCase()) {
									case "REGULAR" :
										regular.deliver(from, to, map);
										break;

									case "SPORTY" :
										sporty.deliver(from, to, map);
										break;

									case "EXCLUSIVE" :
										exclusive.deliver(from, to, map);
										break;

									default:
										System.out.println("Invalid Input : invalid go command");
								}
							} else {
								System.out.println("Invalid Input : you're already in your destination");
							}
						}
					} else {
						System.out.println("Invalid Input : invalid go command");
					}
					break;

				case "-H" :
					System.out.println("Commands :	1. show map");
					System.out.println("			1. go from [START] to [FINISH] with OW-JEK [TYPE]");
					System.out.println("			1. exit");
					break;

				case "EXIT" :
					System.out.println("Thank You for trusting Ow-Jek! See you next time!");
					input = "EXIT";
					break;

				default:
					System.out.println("Invalid Input : command not found");
					input = "";
					break;
			}
		}
	}

	public static boolean checkMap (String from, String to, Map map) {
		try {
			int fromY = Integer.parseInt(((int) from.charAt(0) - 65) + "" + ("" + from.charAt(1)));
			int fromX = Integer.parseInt(((int) from.charAt(2) - 81) + "" + ("" + from.charAt(3)));

			int toY = Integer.parseInt(((int) to.charAt(0) - 65) + "" + ("" + to.charAt(1)));
			int toX = Integer.parseInt(((int) to.charAt(2) - 81) + "" + ("" + to.charAt(3)));

			boolean checker = true;

			if (fromX >= map.WIDTH || fromY >= map.HEIGHT) {
				System.out.println("Invalid Input : " + from + " IS NOT IN THE MAP");
				checker = false;
			}

			if (toX >= map.WIDTH || toY >= map.HEIGHT) {
				System.out.println("Invalid Input : " + to + " IS NOT IN THE MAP");
				checker = false;
			}

			if (!checker) {
				return false;
			}

			if (map.get(fromY, fromX) == '#') {
				System.out.println("Invalid Input : " + from + " IS NOT A ROAD");
				checker = false;
			}

			if (map.get(toY, toX) == '#') {
				System.out.println("Invalid Input : " + to + " IS NOT A ROAD");
				checker = false;
			}

			if (checker) {
				return true;
			}
			return false;
		} catch (NumberFormatException e) {
			System.out.println("Invalid Input : wrong location format");
			return false;
		}
	}
}
