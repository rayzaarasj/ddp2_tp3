import java.util.*;

/**
 * Created by rayzaarasj on 24/04/2017.
 */
public abstract class Owjek {

    public int minYearAllowed;
    public int minTopSpeed;
    public int minCc;
    public int costPerKm;
    public int fixedCost;
    public int firstCostDistKm;
    public double protectionCost;
    public double promo;
    public int promoDistKm;
    public String tipe;

	/**
	 * Get cost of travel
	 * @param distance	: Distanced counted by cost
	 * @return 			: Cost
	 */
	public double getCost(int distance) {
        double distInKM = distance / 10.0;
        return distInKM * this.costPerKm;
    }

	/**
	 * Get promo of travel
	 * @param distance	: Distanced counted for promo
	 * @return			: Promo
	 */
	public double getPromo(int distance) {
        int minPromoInM = this.promoDistKm * 10;
        int afterFixedCost = minPromoInM - (this.firstCostDistKm * 10);
        if (distance >= afterFixedCost) {
            return this.promo * (((this.promoDistKm - this.firstCostDistKm) * this.costPerKm) + this.fixedCost);
        } else {
            double distInKM = distance / 10.0;
            return this.promo * ((distInKM * this.costPerKm) + this.fixedCost);
        }
    }

	/**
	 * Abstract method to be implemented by it's subclass
	 * @param from	: Departure location
	 * @param to	: Arrival location
	 * @param map	: Map used
	 */
    abstract public void deliver(String from, String to, Map map);

	/**
	 * Shortest path algorithm using BFS
	 * Reference : http://stackoverflow.com/questions/10099221/breadth-first-search-on-an-8x8-grid-in-java
	 * @param from	: Departure location
	 * @param to	: Arrival location
	 * @param map	: Map used
	 * @return		: Distance in 100M
	 */
	public int getDistace(String from, String to, Map map){
		int fromY = Integer.parseInt(((int) from.charAt(0) - 65) + "" + ("" + from.charAt(1)));
		int fromX = Integer.parseInt(((int) from.charAt(2) - 81) + "" + ("" + from.charAt(3)));

		int toY = Integer.parseInt(((int) to.charAt(0) - 65) + "" + ("" + to.charAt(1)));
		int toX = Integer.parseInt(((int) to.charAt(2) - 81) + "" + ("" + to.charAt(3)));

		Character[][] copy = new Character[map.HEIGHT][map.WIDTH];

		for (int y = 0; y < map.HEIGHT; y++) {
			for (int x = 0; x < map.WIDTH; x++) {
				copy[y][x] = map.get(y, x);
			}
		}

		map.set('S', fromY, fromX);
		copy[fromY][fromX] = 'S';
		map.set('F', toY, toX);
		copy[toY][toX] = 'F';

		Queue<int[]> queue = new LinkedList<>();
		ArrayList<int[]> visited = new ArrayList<>();
		int[] startQueue = {fromX, fromY, fromX, fromY, 0}; //nowX, nowY, beforeX, beforeY, distance
		queue.add(startQueue);

		while (queue.peek() != null) {

			int[] current = queue.remove();

			//Check Left
			if (current[0] - 1 >= 0 && copy[current[1]][current[0] - 1] != '#') {
				int[] temp = {current[0] - 1, current[1], current[0], current[1], current[4] + 1};
				visited.add(0, temp);

				if (copy[current[1]][current[0] - 1] == 'F') {
					this.createRoute(visited, map);
					return temp[4];
				} else {
					copy[current[1]][current[0] - 1] = '#';
					queue.add(temp);
				}
			}

			//Check Over
			if (current[1] - 1 >= 0 && copy[current[1] - 1][current[0]] != '#') {
				int[] temp = {current[0], current[1] - 1, current[0], current[1], current[4] + 1};
				visited.add(0, temp);

				if (copy[current[1] - 1][current[0]] == 'F') {
					this.createRoute(visited, map);
					return temp[4];
				} else {
					copy[current[1] - 1][current[0]] = '#';
					queue.add(temp);
				}
			}

			//Check Right
			if (current[0] + 1 <= map.WIDTH && copy[current[1]][current[0] + 1] != '#') {
				int[] temp = {current[0] + 1, current[1], current[0], current[1], current[4] + 1};
				visited.add(0, temp);

				if (copy[current[1]][current[0] + 1] == 'F') {
					this.createRoute(visited, map);
					return temp[4];
				} else {
					copy[current[1]][current[0] + 1] = '#';
					queue.add(temp);
				}
			}

			//Check Bellow
			if (current[1] + 1 <=  map.HEIGHT && copy[current[1] + 1][current[0]] != '#') {
				int[] temp = {current[0], current[1] + 1, current[0], current[1], current[4] + 1};
				visited.add(0, temp);

				if (copy[current[1] + 1][current[0]] == 'F') {
					this.createRoute(visited, map);
					return temp[4];
				} else {
					copy[current[1] + 1][current[0]] = '#';
					queue.add(temp);
				}
			}
		}


		return -1;
	}

	/**
	 * Creating dots on map, excluding arrival location
	 * @param visited	: Array of visited coordinates
	 * @param map		: Map used
	 */
	private void createRoute(ArrayList<int[]> visited, Map map) {
	    int[] end = visited.remove(0);
        for (int i = 0; i < visited.size(); i++) {
            int[] x = visited.get(i);
            if (end[2] == x[0] && end[3] == x[1]) {
                int[] temp = visited.remove(i);
                this.drawRoute(temp, visited, map);
                break;
            }
        }
		map.print();
	}

	/**
	 * Recursively create dots by using the array of visited coordinates
	 * @param now		: Now Location
	 * @param visited	: Array of visited coordinates
	 * @param map		: Map used
	 */
	private void drawRoute(int[] now, ArrayList<int[]> visited, Map map) {
        map.set('.', now[1], now[0]);
	    if (now[4] == 1) {
	       return;
        } else {
	        for (int i = 0; i < visited.size(); i++) {
                int[] x = visited.get(i);
                if (now[2] == x[0] && now[3] == x[1]) {
                    int[] temp = visited.remove(i);
                    this.drawRoute(temp, visited, map);
                    break;
                }
            }
        }
    }

}
