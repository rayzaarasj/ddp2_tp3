/**
 * Created by rayzaarasj on 4/25/2017.
 */
public class Sporty extends Owjek {

    public Sporty() {
        minYearAllowed = 2015;
        minTopSpeed = 140;
        costPerKm = 3000;
        fixedCost = 20000;
        firstCostDistKm = 5;
        protectionCost = 0.1;
        promo = 0.6;
        promoDistKm = 8;
        tipe = "Sporty";
    }

	/**
	 * Sporty's implementaion of deliver
	 * @param from	: Departure location
	 * @param to	: Arrival location
	 * @param map	: Map used
	 */
	public void deliver(String from, String to, Map map) {
		int fixedCostInM = this.firstCostDistKm * 10;
		int distance = this.getDistace(from, to, map);
		int countedDistance = (distance >= fixedCostInM) ? distance - fixedCostInM : 0;
		double price = this.getCost(countedDistance);
		double promoPrice;
		promoPrice = this.getPromo(countedDistance);
		double total = this.fixedCost + price - promoPrice;
		System.out.printf("Terimakasih telah melakukan perjalanan dengan OW-JEK%n");
		System.out.printf("[Jarak] %.1f KM%n", distance / 10.0);
		System.out.printf("[TipeO] %s%n", this.tipe);
		System.out.printf("[5KMPe] Rp %d.00 (+)%n", this.fixedCost);
		System.out.printf("[KMSel] Rp %.2f (+)%n", price);
		System.out.printf("[Promo] Rp %.2f (-)%n", promoPrice);
		double protection = total * this.protectionCost;
		System.out.printf("[Prtks] Rp %.2f (+)%n", protection);
		total += protection;
		System.out.printf("[Total] Rp %.2f%n", total);
	}
}
